/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tokokita.tokokita.web;

import com.tokokita.tokokita.entity.Item;
import com.tokokita.tokokita.repository.ItemRepository;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ustadho
 */
@RestController
@RequestMapping("api/item")
public class ItemResouce {
    @Autowired
    ItemRepository itemRepository;
    
    @GetMapping
    public Page<Item> findAll(Pageable pgbl){
        return itemRepository.findAll(pgbl);
    }
    
    @PostMapping
    public void insert(@RequestBody Item item){
        itemRepository.save(item);
    }
    
    @PutMapping
    public void update(@RequestBody Item item){
        itemRepository.save(item);
    }
    
    @DeleteMapping("{id}")
    public void delete(@PathVariable Integer id){
        itemRepository.delete(id);
    }
}
